<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit()
    {
        $user = Auth::user();
        return view('/auth/users/edit', array('user' => $user));
    }

    public function update(Request $request)
    {
        $user = Auth::user();
        $user->display_name = $request-> input('display_name');
        $user->job_name = $request->input('job_name');

        $user->save();
        return redirect('/dashboard');
    }

}
