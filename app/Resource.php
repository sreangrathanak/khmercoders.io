<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    public function contributors()
    {
        return $this->belongsToMany('App\User', 'resource_contributors', 'resource_id', 'user_id');
    }
}
