@extends('master')
@section('title', 'Welcome to Khmer Coders')

@section('content')
    <section class='site-banner'>
        <div class='container'>
            <div class='p-3 py-5' style='max-width: 600px;'>
                <h2 class='mb-4'>Khmer Coders</h2>
                <p>
                    <strong>Khmer Coders</strong> is the biggest Cambodian programmers community.
                    We love each other, we help each other, and we are passionate about new
                    technology.
                </p>

                <p>Come and join the family.</p>

                <a class='btn btn-secondary btn-lg mt-4' href='https://www.facebook.com/groups/1104437376352783/'>Join Us</a>
            </div>
        </div>
    </section>

    <div class='container'>
        <div class='p-3 py-5'>
            <h2 class='mb-4'>Want to help?</h2>
            <p>
                Do you want to make this website better? Join us at: <br>
                <a href='https://gitlab.com/khmercoders/khmercoders.io'>
                https://gitlab.com/khmercoders/khmercoders.io
                </a>
            </p>
        </div>
    </div>
@endsection