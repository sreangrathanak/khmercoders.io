@extends('master')
@section('title', 'Downloads - Khmer Coders')

@section('content')
    @foreach ($resources as $resource)
    <div class='resource container'>
        <div class='resource-content'>
            <h1 class='h4'>{{ $resource->title }}</h1>
            <div style='padding:1rem;'>
                {!! $resource->content !!}
            </div>
        </div>

        <div class='resource-contrib'>
            <strong>Contributors</strong>

            @foreach ($resource->contributors as $contributor)
                <div class='member-profile-list' style='font-size:13px;'>
                    <img style='width:60px; height:60px;' src='{{ $contributor->picture }}' />
                    <div class='summary'>
                        <strong>{{ $contributor->display_name }}</strong><br>
                        <span>{{ $contributor->position_name }}</span><br>
                        <span>{{ $contributor->job_name }}</span>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    @endforeach

@endsection