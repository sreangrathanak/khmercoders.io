@extends('master')
@section('title', 'Khmer Coders Team and Members')

@section('content')
    <div class='container my-5'>
        <h1 class='h4'>Featured Members</h1>
        <div class='member-profile-list-wrap'>

            @foreach ($users as $user)
            <div class='member-profile-list'>
                <img src='{{ $user->getPicture() }}' />
                <div class='summary'>
                    <strong>{{ $user->display_name }}</strong><br>
                    <span>{{ $user->position_name }}</span><br>
                    <span>{{ $user->job_name }}</span>
                </div>
            </div>
            @endforeach

        </div>

        <br><br>

        <h1 class='h4'>Other Members</h1>
        <p>
            Join our <a href='https://www.facebook.com/groups/1104437376352783/'Facebook Group</a> 
            to see the members
        </p>
    </div>
@endsection