You are receiving this email because we received a password reset request for your account.</br>

<a href='{{ $url }}'>{{ $url }}</a>

<br/><br/>

Thanks,</br>
KhmerCoders Support