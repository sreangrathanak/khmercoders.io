@extends('master')
@section('title', 'Khmer Coders Team and Members')

@section('content')
    <div class='container my-5'>
        <h3 class='h5'>Profile Management</h3>
        <ul class='px-5 py-3'>
            <li><a href='/profile/edit'>Edit Your Profile</a></li>
        </ul>
    </div>
@endsection