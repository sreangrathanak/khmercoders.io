<?php

namespace Tests\Unit;

use App\Helper;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class HelperClassTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetAliasFromString()
    {
        $this->assertEquals(Helper::getAliasFromString("Visal .In"), "visal.in");

        $this->assertEquals(
            Helper::getAliasFromString("Mike  Gaetner"), "mike.gaetner",
            "Double space will be replaced by single dot"
        );

        $this->assertEquals(
            Helper::getAliasFromString("Sopheak^_^"), "sopheak",
            "Ignore all the special character."
        );

        $this->assertEquals(
            Helper::getAliasFromString(" Rathpanh@a Sarun^^"), "rathpanha.sarun",
            "Trim and ignore special characters"
        );
    }
}
